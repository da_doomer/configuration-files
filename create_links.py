"""Create symbolic links to the config files in their correct locations

Will create folders if they don't exist"""
from pathlib import Path
import sys
import argparse

locations = {
        "files/vimrc.conf"       : "~/.vimrc",
        "files/fish.conf" : "~/.config/fish/config.fish",
        "files/fishfile.conf"    : "~/.config/fish/fish_plugins",
        "files/tmux.conf"        : "~/.tmux.conf",
        "files/nvim.lua"         : "~/.config/nvim/init.lua",
        "files/wallpaper.jpg" :       "~/.config/wallpaper.jpg",
        "files/gitignore" :       "~/.gitignore",
}

# Ad-hoc argument parsing
actions = "\n".join((f"symlink {b} to {a};" for a, b in locations.items()))
parser = argparse.ArgumentParser(
        description=(
            "Link configuration files to their default locations. "
            "Unless given -f or --force, this script will not overwrite "
            "any existing files."
            ),
        epilog=(
            "The following actions will be attempted when this script is run:"
            "\n\n"
            f"{actions}"
            ),
    )
parser.add_argument(
        "--force", "-f",
        help="Force link creation (overwriting existing files)",
        action="store_const",
        const=True,
        default=False,
    )

args = parser.parse_args()


if __name__ == "__main__":
    # Standarize paths
    orig_dest = {Path(config_file).resolve(): Path(location).expanduser()
                 for config_file, location in locations.items()}

    # Try to create links
    exising_files = list()
    for orig, dest in orig_dest.items():
        if args.force:
            dest.unlink(missing_ok=True)
        if dest.exists():
            exising_files.append((orig, dest))
            continue
        dest.parent.mkdir(parents=True, exist_ok=True)
        dest.symlink_to(orig)
        print("Linked {} -> {}".format(dest, orig.name))

    for orig, dest in exising_files:
        print(f"Did not modify existing {dest}")
    if len(exising_files) > 0:
        print("\nIf you want to overwrite use [--force]")
