# Config files

Run: `$ python create_links.py -h`

Programs configured:
 - fish       (shell)
 - i3         (wm)
 - vim/nvim   (vim implementation)
 - qtile      (wm)
 - termite    (terminal emulator)
 - termux     (android terminal emulator)
 - tmux       (terminal multiplexer)
 - vifm       (command line file manager)
 - Xmodmap    (X11 keyboard modification maps for tildes, right alt mod)


## Assumptions
Its ok if one or more dependencies are not installed

### i3, qtile
 - They call `autostart.sh`, which lists starts GUI applications.
	 Review the file.

### Fish
 - autojump (shell)
 - cowsay, fortune

# Mementos
 - batch renaming                   - E.g. in vifm, ranger, vimv or others

## Other essential programs
 - aRandR                           - Connect to multiple displays!
 - fzf                              - Find files!
 - ack, silver_searcher             - Find files that contain a given text!
 - zathura                          - Light PDF (and epub) viewer.
 - trash-cli                        - Trash CLI interface (you don't always
                                      REALLY mean rm my.config right?
 - earlyoom                         - Linux Out-of-memory killer is no good!
 - illum                            - Control display backlight brightness.
 - powerkit,xfce4-power-manager     - Power managers

 - autojump                         - absolutely essential
 - dasht                            - access documentation from terminal
   zeal,devdocs-desktop,doc-browser
                                    - access documentation from GUI
 - htop, gtop, glances, nvtop, zenith etc.
                                    - variations of the classic htop to suit
                                      your needs
 - asciinema                        - Record terminal
 - peek                             - Record desktop
 - tldr                             - Access program examples from terminal
 - byobu                            - Wrapper around tmux

 - downgrade, agetpkg               - Downgrade packages on arch
 - xdg-mime, mimetype               - View default program for a filetype/mime type
 - lsdesktop, fbrokendesltop        - View available and broken .desktop files
 - nmcli, nmtui                     - CLI network management
 - ncdu                             - CLI disk usage explorer.
 - copyq                            - Clipboard manager applet (xsel, xclip still needed)
 - udiskie                          - Mount storage devices easily (CLI and GUI)
 - arandr, autorandr                - Easy external display configuration.
 - pamac                            - AUR helper with tray applet.
 - yay, trizen                      - Good AUR helpers.
 - caffine                          - Don't turn off the display
 - Simple screen recorder           - A good simple screen recorder
 - chrony, systemd-timesyncd        - Keep you time up-to-date

## Fun
 - cowsay,botsay,fortune,etc        - Display messages with characters, check
	 custom true-color cows
 - asciiplanets, asciiquarium,etc   - CLI animations
 - dunst                            - notification daemon

## Good fonts
 - Iosevka                          - Monospace OpenType font for writing technical
                                      documents, with programming ligatures and
                                      other features.

# Miscellaneous notes

- If using a latop remember to set charging limits to avoid killing your battery.
E.g. using tlp, or
https://askubuntu.com/questions/1006778/set-battery-thresholds-on-ubuntu-asus/1230442#1230442?newreg=dd705c854941476ba3c046122a7484cc

- To enable touchpad on a laptop see: https://wiki.archlinux.org/title/Libinput

	TLDR: see /usr/share/X11/xorg.conf.d/40-libinput.conf if using libinput. Or maybe you are running synapttics.
